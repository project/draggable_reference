<?php

/**
 * Class DraggableReferenceViewsPluginStyle
 */
class DraggableReferenceViewsPluginStyle extends views_plugin_style {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    // Always force using fields even though it's an entity-based row
    $options['uses_fields'] = ['default' => TRUE];
    $options['helper_view'] = '';
    $options['customize'] = [
      'default' => [
        'child_label' => 'Referenced entities',
        'parent_label' => 'Parent entities',
        'unassigned_text' => 'There are no unassigned references',
        'empty_parent' => 'This entity has no references',
        'unassigned_option' => 'No parent',
        'new_parent' => 'New parent',
        'submit' => 'Save parent entities',
        'success_message_singular' => 'One parent entity was saved.',
        'success_message_plural' => '@count parent entities were saved.',
        'failure_message' => 'There were no changes, and no entities have been saved.',
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $view = $form_state['view'];
    $relationships = $view->display_handler->get_option('relationships');
    // Helper view
    $displays = views_get_applicable_views('entityreference display');
    // Filter views that list the entity type we want, and group the separate
    // displays by view.
    $helper_view_options = [];
    $relationship = $relationships[$this->options['relationship']];
    $helper_table = isset($relationship['table']) ? $relationship['table'] : '';

    foreach ($displays as $data) {
      list($view, $display_name) = $data;
      if ((empty($helper_table)) || ($view->base_table === $helper_table)) {
        $helper_view_options[$view->name . ':' . $display_name] = $view->human_name . ' - ' . $view->display[$display_name]->display_title;
      }
    }

    $form['helper_view'] = [
      '#type' => 'select',
      '#title' => t('View used to select all potential parent entities'),
      '#required' => TRUE,
      '#options' => $helper_view_options,
      '#default_value' => $this->options['helper_view'],
    ];

    // Make sure there's only one grouping field
    $form['grouping'] = [reset($form['grouping'])];
    $form['grouping'][0]['field']['#title'] = t('Grouping field');
    $form['grouping'][0]['field']['#required'] = TRUE;
    unset($form['grouping'][0]['rendered'], $form['grouping'][0]['rendered_strip']);
    // Always force using fields even though it's an entity-based row
    $form['uses_fields']['#type'] = 'value';
    $form['uses_fields']['#value'] = TRUE;

    // Interface customization
    $form['customize'] = [
      '#type' => 'fieldset',
      '#title' => t('Customize interface'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['customize']['unassigned_text'] = [
      '#type' => 'textfield',
      '#title' => t('Unassigned references'),
      '#description' => t('Appears under an empty parent, when all available children have been assigned.'),
      '#default_value' => $this->options['customize']['unassigned_text'],
    ];

    $form['customize']['empty_parent'] = [
      '#type' => 'textfield',
      '#title' => t('Unassigned references'),
      '#description' => t('Appears under an empty parent, when all potential children are still available.'),
      '#default_value' => $this->options['customize']['empty_parent'],
    ];

    $form['customize']['parent_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for the “Parent” column'),
      '#description' => t('Header of the column containing the parent select lists.'),
      '#default_value' => $this->options['customize']['parent_label'],
    ];

    $form['customize']['child_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for the “Child” column'),
      '#description' => t('Header of the column containing information on referenced entities.'),
      '#default_value' => $this->options['customize']['child_label'],
    ];

    $form['customize']['unassigned_option'] = [
      '#type' => 'textfield',
      '#title' => t('Unassigned option'),
      '#description' => t('Label for the empty value of the parent select list.'),
      '#default_value' => $this->options['customize']['unassigned_option'],
    ];

    $form['customize']['new_parent'] = [
      '#type' => 'textfield',
      '#title' => t('New parent'),
      '#description' => t('Label for the new-parent-entity field.'),
      '#default_value' => $this->options['customize']['new_parent'],
    ];

    $form['customize']['submit'] = [
      '#type' => 'textfield',
      '#title' => t('Label of submit button'),
      '#default_value' => $this->options['customize']['submit'],
    ];

    $form['customize']['success_message_singular'] = [
      '#type' => 'textfield',
      '#title' => t('Success message: singular form'),
      '#description' => t('Appears when a single entity was saved.'),
      '#default_value' => $this->options['customize']['success_message_singular'],
    ];

    $form['customize']['success_message_plural'] = [
      '#type' => 'textfield',
      '#title' => t('Success message: plural form'),
      '#description' => t('Appears when multiple entities were saved.'),
      '#default_value' => $this->options['customize']['success_message_plural'],
    ];

    $form['customize']['failure_message'] = [
      '#type' => 'textfield',
      '#title' => t('Failure/no action message'),
      '#description' => t('Appears when no entities were saved.'),
      '#default_value' => $this->options['customize']['failure_message'],
    ];
  }

  public function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    foreach ($form_state['values']['style_options']['customize'] as &$value) {
      $value = filter_xss($value, []);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();
    // Make sure the base field exists in the query
    $this->view->query->add_field(
      $this->view->query->base_table,
      $this->view->query->base_field,
      $this->view->query->get_field_alias($this->view->query->base_table, $this->view->query->base_field, $this->view->query->base_field)
    );
  }

  /**
   * @inheritdoc
   */
  public function render_grouping($records, $groupings = [], $group_rendered = NULL) {
    // Make sure fields are rendered
    $this->render_fields($this->view->result);
    $sets = [];

    // Helper view
    $helper_view = $this->options['helper_view'];
    if (!empty($helper_view)) {
      list($helper_view, $helper_display) = explode(':', $helper_view);

      $helper_view = views_get_view($helper_view);
      $helper_view->render($helper_display);
    }

    if ($groupings) {
      // Iterate through configured grouping fields to determine the
      // hierarchically positioned set where the current row belongs to.
      // While iterating, parent groups, that do not exist yet, are added.
      foreach ($groupings as $info) {
        $field = $info['field'];
        $rendered = isset($info['rendered']) ? $info['rendered'] : $group_rendered;
        $rendered_strip = isset($info['rendered_strip']) ? $info['rendered_strip'] : FALSE;

        // Process data from helper view
        if (isset($helper_view->field[$field])) {
          /**
           * @var \EntityDrupalWrapper[] $wrappers
           */
          $wrappers = $helper_view->field[$field]->wrappers;

          foreach ($wrappers as $delta => $wrapper) {
            $sets[$wrapper->getIdentifier()] = [
              'group' => $this->get_group_content($helper_view->style_plugin, $delta, $field, $rendered, $rendered_strip),
              'wrapper' => $wrapper,
              'rows' => [],
            ];
          }
        }

        foreach ($records as $index => $row) {
          $grouping = '';
          $group_content = '';

          if (isset($this->view->field[$field])) {
            $group_content = $this->get_group_content($this, $index, $field, $rendered, $rendered_strip);
          }

          if ($group_content) {
            /**
             * @var \EntityDrupalWrapper $wrapper
             */
            $wrapper = $this->view->field[$field]->wrappers[$index];
            $grouping = $wrapper->getIdentifier();
          }

          $sets[$grouping]['rows'][] = $row;
        }
      }
    }
    else {
      // Create a single group with an empty grouping field.
      $sets[''] = [
        'group' => '',
        'rows' => $records,
      ];
    }

    return $sets;
  }

  /**
   * Gets the group content from a field.
   *
   * @param \views_plugin_style $style
   * @param $index
   * @param $field
   * @param bool $rendered
   * @param bool $rendered_strip
   *
   * @return string
   */
  protected function get_group_content(views_plugin_style $style, $index, $field, $rendered = FALSE, $rendered_strip = FALSE) {
    $group_content = $style->get_field($index, $field);

    if ($this->view->field[$field]->options['label']) {
      $group_content = $this->view->field[$field]->options['label'] . ': ' . $group_content;
    }

    if ($rendered && $rendered_strip) {
      $group_content = strip_tags(htmlspecialchars_decode($group_content));
    }

    return $group_content;
  }

  /**
   * @inheritdoc
   */
  public function render_grouping_sets($sets, $level = 0) {
    module_load_include('inc', 'draggable_reference', 'draggable_reference.pages');
    return drupal_get_form('draggable_reference_form', $sets, $this->view);
  }
}
