<?php

/**
 * Class DraggableReferenceViewsPluginRow
 */
class DraggableReferenceViewsPluginRow extends entity_views_plugin_row_entity_view {

  /**
   * @var array
   */
  public $entity_wrappers = [];

  /**
   * @inheritdoc
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['relationship']['#required'] = TRUE;
    unset($form['relationship']['#options']['none']);
  }

  /**
   * @inheritdoc
   */
  public function pre_render($values) {
    parent::pre_render($values);
    if ($this->entities) {
      foreach ($this->entities as $entity) {
        /**
         * @var \EntityDrupalWrapper $wrapper
         */
        $wrapper = entity_metadata_wrapper($this->entity_type, $entity);
        $this->entity_wrappers[$wrapper->getIdentifier()] = $wrapper;
      }
    }
  }
}
