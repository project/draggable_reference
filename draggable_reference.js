(function ($) {

/**
 * Add functionality to the parents tabledrag enhanced table.
 *
 * This code is almost an exact copy of the code used for the block region and
 * weight settings form.
 */
Drupal.behaviors.referenceDrag = {
  attach: function (context, settings) {
    // tableDrag is required for this behavior.
    if (typeof Drupal.tableDrag == 'undefined' || typeof Drupal.tableDrag.parents == 'undefined') {
      return;
    }

    var table = $('table#parents');
    var tableDrag = Drupal.tableDrag.parents; // Get the blocks tableDrag object.

    // Add a handler for when a row is swapped, update empty regions.
    tableDrag.row.prototype.onSwap = function (swappedRow) {
      checkEmptyPages(table, this);
    };

    // A custom message for the parents page specifically.
    Drupal.theme.tableDragChangedWarning = function () {
      return '<div class="messages warning">' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t("Changes will not be saved until the form is submitted.") + '</div>';
    };

    // Add a handler so when a row is dropped, update fields dropped into new regions.
    tableDrag.onDrop = function() {
      dragObject = this;

      var parentRow = $(dragObject.rowObject.element).prev('tr').get(0);
      var parentName = parentRow.className.replace(/([^ ]+[ ]+)*parent-([^ ]+)-message([ ]+[^ ]+)*/, '$2');
      var parentField = $('select.reference-parent', dragObject.rowObject.element);

      if ($(dragObject.rowObject.element).prev('tr').is('.parent-message')) {
        var weightField = $('select.reference-weight', dragObject.rowObject.element);
        var oldparentName = weightField[0].className.replace(/([^ ]+[ ]+)*reference-weight-([^ ]+)([ ]+[^ ]+)*/, '$2');

        if (!parentField.is('.reference-parent-'+ parentName)) {
          parentField.removeClass('reference-parent-' + oldparentName).addClass('reference-parent-' + parentName);
          weightField.removeClass('reference-weight-' + oldparentName).addClass('reference-weight-' + parentName);
          parentField.val(parentName);
        }
      }
    };

    // Add the behavior to each region select list.
    $('select.reference-parent', context).once('reference-parent', function () {
      $(this).change(function (event) {
        // Make our new row and select field.
        var row = $(this).parents('tr:first');
        var select = $(this);
        tableDrag.rowObject = new tableDrag.row(row);

        // Find the correct region and insert the row as the first in the region.
        $('tr.parent-message', table).each(function () {
          if ($(this).is('.parent-' + select[0].value + '-message')) {
            // Add the new row and remove the old one.
            $(this).after(row);
            // Manually update weights and restripe.
            tableDrag.updateFields(row.get(0));
            tableDrag.rowObject.changed = true;
            if (tableDrag.oldRowElement) {
              $(tableDrag.oldRowElement).removeClass('drag-previous');
            }
            tableDrag.oldRowElement = row.get(0);
            tableDrag.restripeTable();
            tableDrag.rowObject.markChanged();
            tableDrag.oldRowElement = row;
            $(row).addClass('drag-previous');
          }
        });

        // Modify empty regions with added or removed fields.
        checkEmptyPages(table, row);
        // Remove focus from selectbox.
        select.get(0).blur();
      });
    });

    var checkEmptyPages = function(table, rowObject) {
      $('tr.parent-message', table).each(function() {
        // If the dragged row is in this region, but above the message row, swap it down one space.
        if ($(this).prev('tr').get(0) == rowObject.element) {
          // Prevent a recursion problem when using the keyboard to move rows up.
          if ((rowObject.method != 'keyboard' || rowObject.direction == 'down')) {
            rowObject.swap('after', this);
          }
        }
        // This region has become empty
        if ($(this).next('tr').is(':not(.draggable)') || $(this).next('tr').length == 0) {
          $(this).removeClass('parent-populated').addClass('parent-empty');
        }
        // This region has become populated.
        else if ($(this).is('.parent-empty')) {
          $(this).removeClass('parent-empty').addClass('parent-populated');
        }
      });
    };
  }
};

})(jQuery);
