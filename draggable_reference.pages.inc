<?php
/**
 * @file
 * Draggable Reference form.
 */

/**
 * Form constructor for administration page.
 *
 * @param $form
 * @param $form_state
 * @param $sets
 * @param $view
 *
 * @return mixed
 * @see draggable_reference_form_submit()
 */
function draggable_reference_form($form, &$form_state, $sets, $view) {
  // Weights range from -delta to +delta, so delta should be at least half
  // of the amount of references present. This makes sure all references in
  // the same parent get a unique weight.
  $children_count = count($view->result);
  $weight_delta = round(($children_count + 1) / 2);

  $customize = $view->style_plugin->options['customize'];
  // Build parent options list
  $parent_options = [];
  $parent_options[DRAGGABLE_REFERENCE_NONE] = t("- {$customize['unassigned_option']} -");
  $parent_options[DRAGGABLE_REFERENCE_NEW] = t("- {$customize['new_parent']} -");

  array_walk($sets, function ($set) use (&$parent_options, $customize) {
    if (isset($set['wrapper'])) {
      $parent_options[$set['wrapper']->getIdentifier()] = $set['wrapper']->label();
    }
  });

  $form['#tree'] = TRUE;
  // No-entity row
  $form['parents'][DRAGGABLE_REFERENCE_NONE]['title'] = [
    '#markup' => t($customize['unassigned_option']),
  ];

  // New-entity row
  $relationship = $view->style_plugin->row_plugin->options['relationship'];
  $relationship = $view->relationship[$relationship];
  if (isset($relationship->field_info['field_name'])) {
    $draggable_reference_field = $relationship->field_info['field_name'];
  }
  else {
    // Educated assumption
    $draggable_reference_field = str_replace('field_data_', '', $relationship->table_alias);
  }

  $entity_type = $relationship->table;
  $bundles = $relationship->field_info['bundles'][$entity_type];
  $bundles_options = [];

  foreach ($bundles as $bundle) {
    $bundles_options["$entity_type:$bundle"] = $bundle;
  };

  $form['parents'][DRAGGABLE_REFERENCE_NEW]['title'] = [
    '#type' => 'textfield',
    '#title' => t($customize['new_parent']),
  ];

  if (count($bundles_options) == 1) {
    $form['parents'][DRAGGABLE_REFERENCE_NEW]['bundle'] = [
      '#type' => 'value',
      '#value' => key($bundles_options),
    ];
  }
  else {
    $form['parents'][DRAGGABLE_REFERENCE_NEW]['bundle'] = [
      '#type' => 'select',
      '#title' => t('Bundle'),
      '#options' => $bundles_options,
    ];
  }

  // Initial weight
  $weight = -$weight_delta;
  $child_field = $view->base_field;
  $rendered_content = $view->style_plugin->row_plugin->rendered_content;

  foreach ($sets as $parent_id => $parent) {
    $parent_id = empty($parent_id) ? DRAGGABLE_REFERENCE_NONE : $parent_id;
    $children = $parent['rows'];

    $form['parents'][$parent_id] = [
      '#type' => 'container',
    ];

    foreach ($children as $delta => $info) {
      $child_id = $info->{$child_field};
      $child = $rendered_content[$child_id];
      $weight++;

      $form['parents'][$parent_id][$child_id]['info'] = $child;
      $title = $view->style_plugin->row_plugin->entity_wrappers[$child_id]->label();

      $form['parents'][$parent_id][$child_id]['weight'] = [
        '#type' => 'weight',
        '#default_value' => $weight,
        '#delta' => $weight_delta,
        '#title_display' => 'invisible',
        '#title' => t('Weight for @reference', ['@reference' => $title]),
        '#attributes' => [
          'class' => [
            'reference-weight',
            drupal_html_class("reference-weight-{$parent_id}"),
          ],
        ],
      ];
      $form['parents'][$parent_id][$child_id]['parent'] = [
        '#type' => 'select',
        '#default_value' => $parent_id,
        '#title_display' => 'invisible',
        '#title' => t('Parent for @reference', ['@reference' => $title]),
        '#options' => $parent_options,
        '#attributes' => [
          'class' => [
            'reference-parent',
            drupal_html_class("reference-parent-{$parent_id}"),
          ],
        ],
      ];
    }
  }

  // Build the form tree.
  $form['parent_wrappers'] = [
    '#type' => 'value',
    '#value' => array_combine(array_keys($sets), array_column($sets, 'wrapper')),
  ];

  $form['parents_options'] = [
    '#type' => 'value',
    '#value' => $parent_options,
  ];

  $form['rendered_parents'] = [
    '#type' => 'value',
    '#value' => array_combine(array_keys($sets), array_column($sets, 'group')),
  ];

  $form['rendered_parents']['#value'][DRAGGABLE_REFERENCE_NONE] = t($customize['unassigned_option']);
  $form['rendered_parents']['#value'][DRAGGABLE_REFERENCE_NEW] = t($customize['new_parent']);

  $form['draggable_reference_relationship'] = [
    '#type' => 'value',
    '#value' => $relationship,
  ];

  $form['draggable_reference_field'] = [
    '#type' => 'value',
    '#value' => $draggable_reference_field,
  ];

  $form['draggable_reference_customize'] = [
    '#type' => 'value',
    '#value' => $customize,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t($customize['submit']),
  ];

  return $form;
}

/**
 * Produces HTML for draggable_reference_form().
 *
 * @param $variables
 *
 * @return string
 */
function theme_draggable_reference_form($variables) {
  $form = $variables['form'];
  $customize = $form['draggable_reference_customize']['#value'];
  $parents = array_intersect_key($form['parents'], array_flip(element_children($form['parents'])));

  $header = [
    'reference' => t($customize['child_label']),
    'parent' => t($customize['parent_label']),
    'weight' => t('Weight'),
  ];
  $colspan = count($header);

  foreach ($parents as $parent_id => $parent) {
    drupal_add_tabledrag('parents', 'match', 'sibling', 'reference-parent', 'reference-parent-' . $parent_id, NULL, FALSE);
    drupal_add_tabledrag('parents', 'order', 'sibling', 'reference-weight', 'reference-weight-' . $parent_id);

    $children_keys = element_children($parent);
    // Make sure only references count as proper children
    $children_keys = array_filter($children_keys, function ($key) {
      return is_numeric($key);
    });
    $children = array_intersect_key($parent, array_flip($children_keys));

    // Parent title
    $rows["parent_{$parent_id}_title"] = [
      'data' => [
        'title' => [
          'data' => in_array($parent_id, [
            DRAGGABLE_REFERENCE_NONE,
            DRAGGABLE_REFERENCE_NEW,
          ]) ? $form['parents'][$parent_id] : $form['rendered_parents']['#value'][$parent_id],
          'colspan' => $colspan,
        ],
      ],
      'class' => [
        'parent-title',
        drupal_html_class("parent-title-$parent_id"),
      ],
    ];

    // Empty message
    $message = $parent_id <> DRAGGABLE_REFERENCE_NONE ? t($customize['empty_parent']) : t($customize['unassigned_text']);
    $rows["parent_{$parent_id}_message"] = [
      'data' => [
        [
          'data' => '<em>' . $message . '</em>',
          'colspan' => $colspan,
        ],
      ],
      'class' => [
        'parent-message',
        drupal_html_class("parent-{$parent_id}-message"),
        empty($children) ? 'parent-empty' : 'parent-populated',
      ],
    ];

    if (in_array($parent_id, [
      DRAGGABLE_REFERENCE_NONE,
      DRAGGABLE_REFERENCE_NEW,
    ])) {
      // No children for the new parent entity
      continue;
    }

    foreach ($children as $child_id => $info) {
      $rows["parent_{$parent_id}_reference_{$child_id}"] = [
        'data' => [
          'reference' => [
            'data' => $form['parents'][$parent_id][$child_id]['info'],
            'class' => ['info'],
          ],
          'parent' => [
            'data' => $form['parents'][$parent_id][$child_id]['parent'],
            'class' => ['parent'],
          ],
          'weight' => [
            'data' => $form['parents'][$parent_id][$child_id]['weight'],
            'class' => ['weight'],
          ],
        ],
        'class' => [
          'draggable',
          'reference',
        ],
      ];

      $form['parents'][$parent_id][$child_id]['#printed'] = TRUE; // Make sure this doesn't get printed again by drupal_render_children()
    }
  }

  $form['parents'][DRAGGABLE_REFERENCE_NONE]['#printed'] = TRUE;
  $form['parents'][DRAGGABLE_REFERENCE_NEW]['#printed'] = TRUE;

  $table = [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => 'empty',
    '#attributes' => [
      'id' => 'parents',
    ],
    '#attached' => [
      'css' => [
        drupal_get_path('module', 'draggable_reference') . '/draggable_reference.css',
      ],
      'js' => [
        drupal_get_path('module', 'draggable_reference') . '/draggable_reference.js',
      ],
    ],
  ];

  $output = drupal_render($table);
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Submit handler for draggable_reference_form().
 *
 * @param $form
 * @param $form_state
 *
 * @see draggable_reference_form()
 */
function draggable_reference_form_submit($form, &$form_state) {
  // For some reason $form_state['values']['parents'] isn't returning correct
  // weight values, so we use input instead
  $parents = $form_state['input']['parents'];
  $new_parents = [];
  // Build an array of new reference assignments
  foreach ($parents as $parent_id => $children) {
    // Add parent item, just in case it ends up empty
    $new_parents[$parent_id] = !empty($new_parents[$parent_id]) ? $new_parents[$parent_id] : [];

    if ($parent_id <> DRAGGABLE_REFERENCE_NEW) {
      foreach ($children as $child_id => $child) {
        $new_parents[$child['parent']][$child_id] = $child;
      }
    }
    else {
      if (!empty($children['title'])) {
        global $user;
        list($entity_type, $bundle) = explode(':', $form_state['values']['parents'][DRAGGABLE_REFERENCE_NEW]['bundle']);

        try {
          /**
           * @var \EntityDrupalWrapper $entity
           */
          $entity = entity_metadata_wrapper($entity_type);
          // Create new entity and set the wrapper's data with it
          $entity->set(entity_create($entity_type, [
            $entity->entityKey('bundle') => $bundle,
            $entity->entityKey('label') => $children['title'],
            'created' => REQUEST_TIME,
            'changed' => REQUEST_TIME,
            'uid' => $user->uid,
          ]));
        } catch (EntityMetadataWrapperException $e) {
          // @todo catch this properly
        }
      }
    }
  }

  if (isset($entity)) {
    $relationship = $form_state['values']['draggable_reference_relationship'];
    $field_name = $relationship->definition['field_name'];

    if (!empty($new_parents[DRAGGABLE_REFERENCE_NEW])) {
      try {
        $children = $new_parents[DRAGGABLE_REFERENCE_NEW];
        uasort($children, 'drupal_sort_weight');
        $entity->{$field_name}->set(array_keys($children));
        $entity->save();
        $saved[] = $entity->getIdentifier();

        // Making assumptions here. We're mostly safe because it's very likely
        // this is going to be about nodes anyway
        $edit_path = $entity->entityInfo()['default path'] . '/edit';
        $entity_type = $entity->type();
        $form_state['redirect'] = [
          str_replace("%{$entity_type}", $entity->getIdentifier(), $edit_path),
          [
            'query' => drupal_get_destination(),
          ],
        ];

      } catch (EntityMetadataWrapperException $e) {
        // @todo catch this properly
      }
    }
  }

  foreach ($form_state['values']['parent_wrappers'] as $parent_id => $wrapper) {
    if ($wrapper instanceof EntityDrupalWrapper && isset($new_parents[$parent_id])) {
      uasort($new_parents[$parent_id], 'drupal_sort_weight');
      $old_children = element_children($form['parents'][$parent_id]);
      $children = array_keys($new_parents[$parent_id]);

      // Check if the references have changed
      // Compare key/value pairs, regardless of type
      if ($old_children != $children) {
        try {
          $wrapper->{$form_state['values']['draggable_reference_field']} = [];
          $wrapper->{$form_state['values']['draggable_reference_field']} = $children;
          $saved[] = $wrapper->save();
        } catch (EntityMetadataWrapperException $e) {
          // @todo catch this properly
        }
      }
    }
  }

  $customize = $form_state['values']['draggable_reference_customize'];

  if (!empty($saved)) {
    drupal_set_message(format_plural(count($saved), $customize['success_message_singular'], $customize['success_message_plural']));
  }
  else {
    drupal_set_message(t($customize['failure_message']), 'warning');
  }
}
